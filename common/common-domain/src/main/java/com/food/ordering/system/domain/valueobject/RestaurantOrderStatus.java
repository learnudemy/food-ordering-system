package com.food.ordering.system.domain.valueobject;

public enum RestaurantOrderStatus {
    PAID, APPROVED, REJECTED
}
